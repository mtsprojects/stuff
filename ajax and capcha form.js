/*<script src='https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
async defer'></script>*/ //put this in header

//запускаем колбек капчи
var onloadCallback = function() {
    console.log("grecaptcha is ready!");
};

function ajax_combined_seo_c(form){
    var this_form = $(form),

       /* weekDay = new Date().getDay(),
        resultBox = $('.popUp-box.form-result').find('.sub-title_popUp-box');*/ //for day of the week

        container = $(this_form).find('.recaptcha-holder'), // in htmlf form put <div class="recaptcha-holder"></div>
        formId = this_form.attr('data-captcha'); //give form unique id by data-attribute for example

    if (typeof formId!= "undefined" && formId.length>0){ //check if id exists
        var capchaId = 'captcha-' + formId;
    }
    else{
        capchaId = 'captcha-' + this_form.length;
    }
    container.attr('id', capchaId);

    /*var formGoal = $(this_form).attr('data-goal'); //google and yandex goals (made by data-attribute)
    if (typeof formGoal!== "undefined"){
        var formGoalArr = formGoal.split('-'),
            gagoal= formGoalArr[0] + '+' + formGoalArr[1];
    }*/

    var $captchaOptions = {
        'sitekey': '6LeRinoUAAAAALT1YDudecSCeQcf6C7jBMIfjxTO', //capcha key
        'size': 'invisible',
        'badge' : 'bottomleft',
        'callback': function(token) {
            this_form.find(".g-recaptcha-response").val(token);
            var data = this_form.serialize();
            $.ajax({
                type: "POST",
                url: "../send/combined_seo.php",
                data: this_form.serialize(),
                success: function (data) {
                    data.status = typeof data.status === 'undefined' ? 0 : parseInt(data.status);
                    switch (data.status) {
                        case 0:
                            alert(data.error);
                            break;
                        case 1:
                           /* if (typeof formGoal!== "undefined" && formGoal.length>0){
                                yaCounter15045442.reachGoal(formGoal);
                                ga('send', 'event', 'form', 'submit', gagoal);
                            }*/ //google and yandex goals push
                            $('.popUp-box').hide(0); //hiding this form popup conyainer
                            this_form.next(".results").html(data.results); //not shure
                            $('.popUp-box.form-result').show(0, function() { //displaying popup with thkyou msg
                              /*  if (weekDay === 6 || weekDay === 0){
                                    $(resultBox).html("Сегодня у нас выходной день.<br/> Наш менеджер свяжется с Вами в течение 15 минут в начале следующего рабочего дня. <br/>Хороших выходных!");
                                }*/ //if weekend show additional msg
                            });
                            this_form[0].reset(); //reset form
                            grecaptcha.reset(cRender); //reset capcha (not shure if works)
                            console.log(grecaptcha.getResponse(cRender));
                            break;
                    }
                },
                error: function (xhr, str) {
                    alert("Неизвестная ошибка, пожалуйста, попроверьте все поля или позвоните нам.");
                    console.log(this_form.serialize());
                    this_form[0].reset();
                }
            });
        }
    };

    var capchaBox = this_form.find('.grecaptcha-badge');
    if (typeof capchaBox === "undefined" || capchaBox.length === 0){
        var cRender =  grecaptcha.render(capchaId, $captchaOptions);
        grecaptcha.execute(cRender);
        console.log('render capcha' + capchaId);
    }
    else{
        console.log('capcha already exists ' + capchaId);
        grecaptcha.execute();
    }
}

